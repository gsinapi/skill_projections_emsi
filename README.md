
**Run Code**

- install packages in the setup.py. Make sure to install the snowflake-helpers package. You can create a new conda environment named 'snowflake' and install all the needed packages
- activate the new conda environment
- in the command_line.py file (in the skill_projections folder), modify the destination schema of the tables in the main(), namely development_schema
- Also modify the snowflake configuration file (skills_projections/templates/snowflake_config) to make sure to include the correct credentials for the connection
- Run the code as follows:
%run skills_projections/command_line_emsi.py

**Model Pipeline**

 The following steps illustrates the tables produced during the model pipeline, using the homonym sql queries included in the folder skill_projections/sql:

***SKILL_VARIABLES***: this table contains a (random generated) variable_id for each skill in each geography

***MONTHLY_SKILL_COUNTS***: this table contains monthly counts for every variable_id (where a variable_id represents a skill available in all geographies. The variable_id for the same skills in different geographies should be different.). The date format is year_month (for example, 201901 for January 2019). The table includes also other metrics, such as a rolling average and a deviation from the rolling average, which are used to calculate the adjusted job posting count.

***SKILL_QUALITY_METRICS***: this table contains quality metrics for every variable_id and it is based on the monthly skill counts. The quality metrics determine the ‘goodness’ of a skills, which means whether a skill passes or not the quality checks and is eligible to be projected.  

***METADATA***: table that contains quality metrics to determine goodness of skills, as well as first valid dates (year/month). The quality metrics  determine whether a skill has passed quality checks.  

***PREPARED INPUT***: table that creates the input for the SVR model, producing an aggregate index for each skill to be projected, created using the job postings count by year/month for the skills that passed the quality checks.


***PREDICTED LEVEL SVR***: train the machine learning model and predict the aggregate index for each year/month using SVR model.  

***PREDICTED GROWTH SVR***: Using the output from svr model, this step creates the growth rates (1,2,3,5 years growth), calculating the percentage difference between the predicted values and base values (based on the aggregate index).  
