/*
Last Modification Date: 2021-01-09
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

*/

select
b.*,
a.job_postings_count,
b.job_postings_count_defined_occupations / a.job_postings_count as
  defined_occupations_share
from
(select
a.prediction_id,
sum(a.job_postings_count) as job_postings_count
from
skill_occupation_counts as a
group by prediction_id) as a
inner join
(select
a.prediction_id,
sum(job_postings_count * occupation_one_year_growth) /
   sum(job_postings_count) as one_year_growth,
sum(job_postings_count * occupation_two_years_growth) /
   sum(job_postings_count) as two_years_growth,
sum(job_postings_count * occupation_three_years_growth) /
   sum(job_postings_count) as three_years_growth,
sum(job_postings_count * occupation_five_years_growth) /
   sum(job_postings_count) as five_years_growth,
array_agg(a.occupation) within group (order by a.occupation_rank)
  as defined_occupations,
array_agg(a.occupation_rank) within group (order by a.occupation_rank)
  as defined_occupation_ranks,
array_size(defined_occupations)  as defined_occupations_count,
min(occupation_rank) as best_defined_occupation_rank,
max(occupation_rank) as worst_defined_occupation_rank,
sum(job_postings_count * svr_prediction_deviation)
  / sum(job_postings_count) as svr_prediction_deviation,
sum(job_postings_count) as job_postings_count_defined_occupations
/*(
case when one_year_growth is null then 1 else
  case when occupation_one_year_growth >= 0 and
    one_year_growth >= 0 or occupation_one_year_growth <= 0
    and one_year_growth <= 0 then least(abs(abs(one_year_growth) /
    abs(occupation_one_year_growth) / 20) , 1) else 1 end end))*/
from
skill_occupation_counts as a
inner join occupation_defining_skill as b
on a.prediction_id = b.prediction_id and a.occupation = b.occupation
group by a.prediction_id) as b
on a.prediction_id = b.prediction_id
