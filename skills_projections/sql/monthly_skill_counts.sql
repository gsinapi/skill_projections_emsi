/*
Last Modification Date: 2021-01-19
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.
alter session set WEEK_START=7;
*/




with date_dimension as (
select
distinct(substring(replace(POSTED::string,'-'),1,6)) as YEAR_MONTH
from EMSI.GLOBAL.POSTINGS where year_month>200000),
 monthly_count_postings as (
select
nation as geography,
to_number(to_char(posted,'YYYYMM'),'999999') as job_posting_year_month,
replace(b.value, '"', '') as variable_name,
'skill' as variable_type,
count(*) as job_postings_count
from
EMSI.GLOBAL.POSTINGS as a
inner join lateral flatten(skills_name) as b
where
variable_name is not null
group by geography, job_posting_year_month, variable_name, variable_type),
base_table as (
select a.variable_id,
b.geography,
b.variable_type,
b.variable_name,
b.job_posting_year_month as job_posting_year_month,
b.job_postings_count
from
skill_variables as a
inner join monthly_count_postings as b
on a.geography = b.geography
and a.variable_type = b.variable_type
and a.variable_name = b.variable_name),

max_year_month as
(
select
201401 as first_year_month,
100 * year(dateadd(month, -1, CURRENT_DATE()))
  + month(dateadd(month, -1, CURRENT_DATE())) as
  last_year_month
),
subset as
(
select
b.variable_id,
a.year_month as job_posting_year_month,
(case when c.job_postings_count is not null then c.job_postings_count
  else 0 end)::number(38, 0) as job_postings_count
from
(select distinct year_month from
 date_dimension) as a
inner join max_year_month as d on true
inner join (select distinct variable_id from skill_variables)
  as b on true
left join base_table as c
on a.year_month = c.job_posting_year_month
  and b.variable_id = c.variable_id
where a.year_month >= d.first_year_month and
  a.year_month <= d.last_year_month
),

adjusted as
(
select
a.*,
(case when b.job_postings_count is not null
  and b.job_postings_count > 0 then
  a.job_postings_count / b.job_postings_count else
  0 end)::float as job_postings_count_adjusted
from
subset as a
left join
(select  to_number(to_char(posted,'YYYYMM'),'999999') as job_posting_year_month,
count(*) as job_postings_count
   FROM EMSI.GLOBAL.POSTINGS as ld
     group by job_posting_year_month) as b
on a.job_posting_year_month = b.job_posting_year_month),

deviation as
(
select
*,
(avg(job_postings_count) over (partition by variable_id
  order by job_posting_year_month rows between 3 preceding and 3 following))
  ::float as rolling_average,
(case when rolling_average > 0 then
  job_postings_count / rolling_average - 1
  else case when job_postings_count > 0 then 10000 else null end end)
  ::float as relative_deviation_from_rolling_average
from
adjusted
)
select
*
from
deviation
