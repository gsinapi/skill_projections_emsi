/*
Last Modification Date: 2021-01-25
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

*/

with base_table as
(
select
case when a.prediction_id is not null then a.prediction_id
  else b.prediction_id end as prediction_id,
a.one_year_growth as one_year_growth_derived,
a.two_years_growth as two_years_growth_derived,
a.three_years_growth as three_years_growth_derived,
a.five_years_growth as five_years_growth_derived,
b.one_year_growth as one_year_growth_svr,
b.two_years_growth as two_years_growth_svr,
b.three_years_growth as three_years_growth_svr,
b.five_years_growth as five_years_growth_svr,
a.svr_prediction_deviation,
a.defined_occupations_share,
defined_occupations_share * one_year_growth_derived +
  (1 - defined_occupations_share) * one_year_growth_svr
  as one_year_growth_combined,
defined_occupations_share * two_years_growth_derived +
  (1 - defined_occupations_share) * two_years_growth_svr
  as two_years_growth_combined,
defined_occupations_share * three_years_growth_derived +
  (1 - defined_occupations_share) * three_years_growth_svr
  as three_years_growth_combined,
defined_occupations_share * five_years_growth_derived +
  (1 - defined_occupations_share) * five_years_growth_svr
  as five_years_growth_combined,
case when one_year_growth_derived is null or
  (one_year_growth_svr is not null and
  a.svr_prediction_deviation <=
  $skill_predictor_svr_prediction_deviation_cutoff)
  then 'svr' else case when one_year_growth_derived is not null
  and one_year_growth_svr is not null and
  a.svr_prediction_deviation >
  $skill_predictor_svr_prediction_deviation_cutoff
  and one_year_growth_svr > 0 and
  one_year_growth_derived < one_year_growth_svr and (one_year_growth_svr - one_year_growth_derived) > 0.04
then 'combined' else 'derived' end end as
  prediction_method_candidate,
case when prediction_method_candidate = 'svr' then
  one_year_growth_svr else case when
  prediction_method_candidate = 'combined' then
  one_year_growth_combined
else one_year_growth_derived end end as one_year_growth_candidate,
case when prediction_method_candidate = 'svr' then
  two_years_growth_svr else case when
  prediction_method_candidate = 'combined' then
  two_years_growth_combined
else two_years_growth_derived end end as two_years_growth_candidate,
case when prediction_method_candidate = 'svr' then
  three_years_growth_svr else case when
  prediction_method_candidate = 'combined' then
  three_years_growth_combined
else three_years_growth_derived end end as three_years_growth_candidate,
case when prediction_method_candidate = 'svr' then
  five_years_growth_svr else case when
  prediction_method_candidate = 'combined' then
  five_years_growth_combined
else five_years_growth_derived end end as five_years_growth_candidate
from
predicted_growth_derived as a
full outer join predicted_growth_svr as b
on a.prediction_id = b.prediction_id
),

processed_override as
(
select
b.prediction_id,
c.prediction_id as related_prediction_id,
(case when a.related_prediction_weight is not null then
  a.related_prediction_weight else 1 end)::float
  as related_prediction_weight,
case when a.add_noise is not null then a.add_noise else true
  end as add_noise
from
override as a
inner join metadata as b
on a.geography = b.geography and a.variable_type = b.variable_type
  and a.variable_name = b.variable_name
inner join metadata as c
  on case when a.related_geography is not null then a.related_geography
  else a.geography end = c.geography and
  case when a.related_variable_type is not null
  then a.related_variable_type else a.variable_type end
  = c.variable_type and
  case when a.related_variable_name is not null
  then a.related_variable_name else a.variable_name end
  = c.variable_name
)

select
c.variable_name,
c.variable_type,
c.geography,
a.*,
b.related_prediction_id,
b.related_prediction_weight,
b.one_year_growth_related,
b.two_years_growth_related,
b.three_years_growth_related,
b.five_years_growth_related,
case when add_noise then
  uniform(-30000, 30000, random($skill_predictor_seed)) / 1000000
else 0 end as noise_factor,
case when b.one_year_growth_related is not null then
  'override' else a.prediction_method_candidate end
  as prediction_method,
c.market_growth_adjustment,
(case when b.one_year_growth_related is not null
  then b.related_prediction_weight * b.one_year_growth_related
  + (1 - b.related_prediction_weight) * a.one_year_growth_candidate
  else a.one_year_growth_candidate end
    + c.market_growth_adjustment) * (1 + noise_factor) as one_year_growth,
(case when b.two_years_growth_related is not null
  then b.related_prediction_weight * b.two_years_growth_related
  + (1 - b.related_prediction_weight) * a.two_years_growth_candidate
  else a.two_years_growth_candidate end
  + c.market_growth_adjustment) * (1 + noise_factor)  as two_years_growth,
(case when b.three_years_growth_related is not null
  then b.related_prediction_weight * b.three_years_growth_related
  + (1 - b.related_prediction_weight) * a.three_years_growth_candidate
  else a.three_years_growth_candidate end
  + c.market_growth_adjustment) * (1 + noise_factor) as three_years_growth,
(case when b.five_years_growth_related is not null
  then b.related_prediction_weight * b.five_years_growth_related
  + (1 - b.related_prediction_weight) * a.five_years_growth_candidate
  else a.five_years_growth_candidate end
  + c.market_growth_adjustment) * (1 + noise_factor) as five_years_growth
from base_table as a
left join
(
select
a.*,
b.one_year_growth_candidate as one_year_growth_related,
b.two_years_growth_candidate as two_years_growth_related,
b.three_years_growth_candidate as three_years_growth_related,
b.five_years_growth_candidate as five_years_growth_related
from
processed_override as a
inner join base_table as b
on a.related_prediction_id = b.prediction_id
) as b on a.prediction_id = b.prediction_id
inner join metadata as c
on a.prediction_id = c.prediction_id
