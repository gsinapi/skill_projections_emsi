/*
Last Modification Date: 2021-01-25
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

*/


select
a.variable_id as prediction_id,
a.variable_id as variable_id_bgt,
a.geography,
a.variable_type,
a.variable_name,
a.variability as variability_bgt,
a.sparsity as sparsity_bgt,
a.availability as availability_bgt,
a.reliability as reliability_bgt,
a.first_valid_year_month as first_valid_year_month_bgt,
a.passed_quality_checks as use_bgt_data,
case when a.passed_quality_checks then
  greatest(a.first_valid_year_month, a.first_valid_year_month) else
  a.first_valid_year_month end as first_used_year_month_original,
case when first_used_year_month_original is null then e.first_valid_date else first_used_year_month_original end first_used_year_month,
a.passed_quality_checks as passed_quality_checks,
0.03 as market_growth_adjustment,
d.avg_posting_count,
e.first_valid_date
from
skill_quality_metrics as a
inner join (select variable_id, avg(job_postings_count) as avg_posting_count
     from monthly_skill_counts
     group by variable_id) as d
  on a.variable_id = d.variable_id
  left join (select variable_id, min(job_posting_year_month) as first_valid_date from monthly_skill_counts
       where abs(relative_deviation_from_rolling_average) <= 0.3 and job_postings_count > 2
      group by variable_id
   ) as e
       on a.variable_id = e.variable_id
