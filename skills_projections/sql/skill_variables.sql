/*
Last Modification Date: 2021-01-19
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

*/

with skill_variables_atlas as (
select
nation as geography,
replace(b.value, '"', '') as variable_name,
count(*) as job_postings_count
from
EMSI.GLOBAL.POSTINGS as a
inner join lateral flatten(skills_name) as b
where
variable_name is not null
group by geography, variable_name)
select geography, variable_name, 'skill' as variable_type,
row_number()
  over (order by geography desc) as variable_id
  from skill_variables_atlas as a  
