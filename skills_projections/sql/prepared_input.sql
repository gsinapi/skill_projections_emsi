/*
Last Modification Date: 2021-01-09
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

where (b.passed_quality_checks or b.reliability_bgt >= 0.3 or b.avg_posting_count > 1) and year_month >= b.first_used_year_month

all skills: (b.passed_quality_checks in (True, False) or b.reliability_bgt >= 0.3)
backtesting add:  where a.year_month <202000
*/

with bgt_counts as
(
select
a.variable_id,
a.job_posting_year_month as year_month,
(case when b.first_year_month_count > 0 then
  100 * a.job_postings_count_adjusted / b.first_year_month_count
  else 10000 end)::float as bgt_index
from monthly_skill_counts as a
inner join
(select
b.variable_id_bgt,
b.geography,
b.variable_type,
b.variable_name,
a.job_postings_count_adjusted as first_year_month_count,
b.first_used_year_month,
b.passed_quality_checks,
b.avg_posting_count,
b.reliability_bgt
from monthly_skill_counts as a
inner join metadata as b
on a.variable_id = b.variable_id_bgt
  and a.job_posting_year_month = b.first_used_year_month) as b
on a.variable_id = b.variable_id_bgt
where (b.passed_quality_checks or b.reliability_bgt >= 0.3) and year_month >= b.first_used_year_month
)

select
a.variable_id as prediction_id,
a.year_month,
a.bgt_index,
(a.bgt_index)::float as
  aggregate_index
from
bgt_counts as a
