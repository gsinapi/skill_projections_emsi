/*
Last Modification Date: 2021-01-19
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

*/

select
a.variable_id,
a.variability::float as variability,
a.sparsity::float as sparsity,
b.first_valid::number(6, 0) as first_valid_year_month,
(case when first_valid is not null then
  timestampdiff(month,
                to_date(b.first_valid::varchar, 'YYYYMM'),
                to_date(a.last_year_month::varchar, 'YYYYMM'))
   + 1 else 0 end)::number(38, 0) as valid_months_count,
least(
  (case when valid_months_count >= 12 then
   valid_months_count else 0 end)
  / 72, 1)::float as availability,
(case when availability >= 0.3333 then
  (1 - (6 * variability + 2 * sparsity +
  (1 - availability)) / 9) else 0 end)
  ::float as reliability,
reliability >= 0.5 as passed_quality_checks,
c.geography,
c.variable_type,
c.variable_name
from
(select
variable_id,
avg(least(
  (case when abs(relative_deviation_from_rolling_average)
  > 0.1 then abs(relative_deviation_from_rolling_average)
  else 0 end) / 0.5, 1)) as variability,
1 - avg(least(
  (case when job_postings_count >= 10 then job_postings_count else 0 end)
  / 100, 1)) as sparsity,
min(job_posting_year_month) as first_year_month,
max(job_posting_year_month) as last_year_month
from
monthly_skill_counts
group by variable_id
) as a
left join
(select
variable_id,
min(job_posting_year_month) as first_valid
from
monthly_skill_counts
where abs(relative_deviation_from_rolling_average) <= 0.3
and job_postings_count >= 10
group by variable_id) as b on a.variable_id = b.variable_id
inner join
skill_variables as c on a.variable_id = c.variable_id
