#!/usr/bin/env python
#
# Last Modification Date: 2021-01-09
# Author: Gilberto Noronha <gnoronha@burning-glass.com>
#
# Please see the "README" file for basic installation and usage
# instructions.
#

import os

import numpy as np
import pandas as pd

import pyarrow.feather as pf
import pyarrow.parquet as pq

import snowflake_helpers as snow


def get_query():
    '''
    query Atlas posting table
    '''
    query = f"""select * from EMSI.GLOBAL.POSTINGS limit 10"""
    return query


connection = snow.connect(user="GIOVANNISINAPI", account="emsi.us-east-1", password="ColinLawry123456789",
                          role="DATA_TEAM", warehouse="COMPUTE_WH", database='EMSI')

# connection = snow.connect(user="gsinapi@burning-glass.com", account="wv35338.us-east-1", password="zCq6EpHa1y",
#                           role="bgt_analytics_ro", warehouse="bgt_analytics", database='BGT_DATA_34')


query = get_query()
# snow.snowflake_to_parquet(query, connection, output_parquet_file,
#                              chunk_size=5000000)
df = snow.from_snowflake(
    sql="select * from EMSI.GLOBAL.POSTINGS limit 10", connection=connection)
